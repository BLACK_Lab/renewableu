(function ($) {

    var $body = $('body');
    var $window = $(window);

    $(window).trigger('resize');

    setTimeout(function () {
        $('#preloader').removeClass('active');
    }, 1000);

    // Elementor pagination wrap
    $( ".elementor-pagination" ).wrap( "<div class='nav-links'></div>" );
    $( ".elementor-pagination .page-numbers" ).wrapInner( "<span class='numbers-wrap'></span>");

    // SVG color chang
    jQuery('img.img-svg').each(function(){
        var $img = jQuery(this);
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        jQuery.get(imgURL, function(data) {
            var $svg = jQuery(data).find('svg');
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
            $img.replaceWith($svg);
        }, 'xml');
    });

})(jQuery);